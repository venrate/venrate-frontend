import CeramicClient from "@ceramicnetwork/http-client";
import { ThreeIdConnect, EthereumAuthProvider } from "@3id/connect";
import { definitions } from "../config.json";
import { IDX } from "@ceramicstudio/idx";
const API_URL = "https://ceramic-clay.3boxlabs.com";
const ceramic = new CeramicClient(API_URL);
import axios from "axios";
import KeyDidResolver from "key-did-resolver";
import ThreeIdResolver from "@ceramicnetwork/3id-did-resolver";

import { DID } from "dids";
const resolver = {
  ...KeyDidResolver.getResolver(),
  ...ThreeIdResolver.getResolver(ceramic)
};
const did = new DID({ resolver });
ceramic.did = did;

//Tile Doc
import { TileDocument } from "@ceramicnetwork/stream-tile";

//Web3 for ENS
import { Eth } from "web3";

export const state = () => ({
  ethaddress: "",
  did: "",
  idx: {},
  authenticated: false,
  profile: {},
  profilePic: "",
  venrateProfile: {},
  repScore: {},
  repScoresHistory: [],
  name: "",
  age: "",
  PAN: "",
  bio: "",
  ens: ""
});

export const mutations = {
  setAuth(state, payload) {
    state.ethaddress = payload.ethaddress;
    // localStorage.setItem("ethaddress", payload.ethaddress);

    state.did = payload.did;
    // localStorage.setItem("did", payload.did);

    state.idx = payload.idx;
    // localStorage.setItem("idx", JSON.stringify(payload.idx));

    state.authenticated = true;
    localStorage.setItem("authenticated", true);

    console.log("Mutation executed and state updated");
  },
  setBasicProfile(state, payload) {
    if (payload.profile != null) {
      state.profile = payload.profile;

      // let cid = payload.basicProfile.image.original.src.slice(7);

      // localStorage.setItem("profilePic", state.profilePic);
      // localStorage.setItem("basicProfile", JSON.stringify(payload.profile));
    }
  },
  setVenrateProfile(state, payload) {
    if (payload.venrateProfile != null) {
      state.venrateProfile = payload.venrateProfile;
      // state.profilePic = payload.venrateProfile.profilePic;
    }
  },
  setRepScore(state, payload) {
    if (payload.repScore != null) {
      state.repScore = payload.repScore;
    }
  },
  setRepScoresHistory(state, payload) {
    if (payload.repScoresHistory != null) {
      state.repScoresHistory = payload.repScoresHistory;
    }
  },
  logout(state) {
    state.ethaddress = "";
    localStorage.setItem("ethaddress", "");

    state.did = "";
    localStorage.setItem("did", "");

    state.profile = {};
    localStorage.setItem("basicProfile", {});

    state.authenticated = false;
    localStorage.setItem("authenticated", false);
    localStorage.setItem("reputation", "");

    console.log("User Logged out Succesfully");
  }
};
export const actions = {
  async ceramicAuth({ commit, dispatch }) {
    await ceramic.setDID(did);
    const addresses = await window.ethereum.enable();
    const threeIdConnect = new ThreeIdConnect();
    const authProvider = new EthereumAuthProvider(
      window.ethereum,
      addresses[0]
    );
    await threeIdConnect.connect(authProvider);
    const provider = await threeIdConnect.getDidProvider();
    ceramic.did.setProvider(provider);
    console.log(provider);
    var res = await ceramic.did.authenticate();
    console.log(res);
    console.log("set");
    console.log(ceramic.did.id);
    //IDX Authentication
    const idx = new IDX({ ceramic, aliases: definitions });
    console.log("idx");
    if (idx.authenticated) {
      console.log("authenticated IDX!!");
      localStorage.setItem("did", ceramic.did.id);
      localStorage.setItem("ethaddress", addresses[0]);
      let payload = {
        idx,
        ethaddress: addresses[0],
        did: ceramic.did.id
      };
      // let payload = {
      //   idx,
      //   ethaddress: localStorage.getItem("ethaddress"),
      //   did: localStorage.getItem("did")
      // };
      setTimeout(() => {
        commit("setAuth", payload);
      }, 1000);

      setTimeout(() => {
        dispatch("fetchBasicProfile");
      }, 1200);

      return true;
    } else {
      return false;
    }
  },

  async venrateAuthenticated({ state, dispatch }) {
    //Authentication has to take place once again in case the page has been refreshed
    //we check the important varibales of idx and ceramic to confirm
    // if (
    //   ceramic.did.id == null ||
    //   ceramic.did.id == "" ||
    //   state.idx == {} ||
    //   state.idx == null ||
    //   !state.idx.authenticated
    // )
    if (state.idx == {} || state.idx == null || !state.idx.authenticated) {
      await dispatch("ceramicAuth");
    }
    return true;
  },

  async fetchBasicProfile({ state, commit, dispatch }) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      const profile = await idx.get("basicProfile");
      console.log("Basic Profile");
      console.dir(profile);
      setTimeout(() => {
        commit("setBasicProfile", { profile });
      }, 500);
      console.log("Basic Profile Action Executed");
    }
  },

  async fetchVenrateProfile({ state }) {
    const idx = state.idx;
    const venrateProfile = await idx.get("venrateProfile");
    console.log("Venrate Profile");
    console.dir(venrateProfile);
    setTimeout(() => {
      commit("setVenrateProfile", { venrateProfile });
    }, 1000);
    console.log("Venrate Profile Action Executed");
  },

  async fetchVenrateReputation({ state, dispatch }) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      //replace with the alias of the reputation document
      const repScore = await idx.get("venrateRepScore");
      console.log("Reputation Score");
      console.dir(repScore);
      setTimeout(() => {
        commit("setRepScore", { repScore });
      }, 1000);
      console.log("Reputation Score Action Executed");
    }
  },

  async generateReputationScore({ state }) {
    //algorithm which calculates score with etherscan
    //axios call to ping the venrate node js backend
    if (await dispatch("venrateAuthenticated")) {
      let ethaddress = state.ethaddress;
      const repScore = await axios.get(
        `https://reputation-api.herokuapp.com/reputation/${ethaddress}`
      );
      state.repScore = repScore;
    }
    localStorage.setItem("repScore", repScore);
  },

  async fetchHistoricReputationScores({ state }) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      //replace with the alias of the reputation history document
      const repScoresHistoryDoc = await idx.get("venrateHistoricRepScores");
      console.log("Reputation Scores History");
      console.dir(repScoresHistoryDoc);
      if (
        repScoresHistoryDoc != null &&
        repScoresHistoryDoc.records.length > 0
      ) {
        const repScoresHistory = repScoresHistoryDoc.records;
        setTimeout(() => {
          commit("setRepScoresHistory", { repScoresHistory });
        }, 1000);
      }
      console.log("Historic Reputation Scores Action Executed");
    }
  },

  async updateVenrateProfile({ state, dispatch }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      // console.log(payload.selectedImage)

      // let imgurl = await venrateInfuraUpload(payload.selectedImage)

      // payload.profile.profilePic = imgurl
      console.log(payload.profile);

      const profile = await idx.set("venrateProfile", payload.profile);
      console.log(profile);

      setTimeout(() => {
        //refresh and update the stored profile in state
        dispatch("fetchVenrateProfile");
      }, 2000);

      // setTimeout(() => {
      //   commit('updateProfile', { profile: profile })
      // }, 2000)
    }
  },
  async updateBasicProfile({ state, dispatch }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      console.log(payload.selectedImage);

      // let imgurl = await venrateInfuraUpload(payload.selectedImage)

      // payload.profile.profilePic = imgurl
      console.log(payload.profile);

      const profile = await idx.set("basicProfile", payload.profile);
      console.log(profile);

      setTimeout(() => {
        //refresh and update the stored profile in state
        dispatch("fetchBasicProfile");
      }, 2000);

      // setTimeout(() => {
      //   commit('updateProfile', { profile: profile })
      // }, 2000)
    }
  },

  async createRepScoreCeramicDoc({ state, dispatch }, payload) {
    let strmurl = null;
    if (await dispatch("venrateAuthenticated")) {
      const doc = await TileDocument.create(ceramic, payload.repScore, {
        controllers: [ceramic.did.id],
        family: "venrate reputation doc family"
      });

      const streamId = doc.id.toString();
      strmurl = streamId;
      return strmurl;
    }
    return strmurl;
  },

  async readHistoricRepScores({ dispatch }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const streamId = payload.DocID; // Replace this with the StreamID of the TileDocument to be loaded
      const doc = await TileDocument.load(ceramic, streamId);
      return doc;
    }
    return null;
  },

  async updateReputationScore({ state }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      console.log(payload.repScore);

      const repScore = await idx.set("venrateRepScore", payload.repScore);
      console.log(repScore);

      await dispatch("fetchHistoricReputationScores");

      setTimeout(() => {
        //update the history
        //refresh and update the stored current reputation in state
        dispatch("updateRepHistory", payload);
        dispatch("fetchVenrateReputation");
      }, 2000);
    }
  },

  async updateRepHistory({ state, dispatch }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      const DocID = await dispatch(
        "createRepScoreCeramicDoc",
        payload.repScore
      );
      var today = new Date();
      var date = today.toISOString();

      if (state.repScoresHistory.length > 0) {
        const repScoresHistoryDoc = await idx.set("venrateHistoricRepScores", {
          records: [{ id: DocID, date: date }, ...state.repScoresHistory]
        });
        console.log(repScoresHistoryDoc);
      } else if (
        state.repScoresHistory == null ||
        state.repScoresHistory.length <= 0
      ) {
        const repScoresHistoryDoc = await idx.set("venrateHistoricRepScores", {
          records: [{ id: DocID, date: date }]
        });
        console.log(repScoresHistoryDoc);
      }
      setTimeout(() => {
        //update the history in state
        dispatch("fetchHistoricReputationScores");
      }, 2000);
    }
  },

  async searchOtherProfileByEthaddress({ state, commit, dispatch }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      let caip_10_acc_id = payload.ethaddress + "@eip155:1";
      let venProfile = null;
      venProfile = await idx.get("basicProfile", caip_10_acc_id);
      return venProfile;
    }
  },

  async searchOtherProfileByDID({ state, commit, dispatch }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      let did = payload.did;
      let venProfile = null;
      venProfile = await idx.get("basicProfile", did);
      return venProfile;
    }
  },

  async searchOtherProfileReputationByEthaddress({ state, commit, dispatch }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      let caip_10_acc_id = payload.ethaddress + "@eip155:1";
      let venrateRepScore = null;
      venrateRepScore = await idx.get("venrateRepScore", caip_10_acc_id);
      return venrateRepScore;
    }
  },

  async searchOtherProfileReputationByDID({ state, commit }, payload) {
    if (await dispatch("venrateAuthenticated")) {
      const idx = state.idx;
      let did = payload.did;
      let venrateRepScore = null;
      venrateRepScore = await idx.get("venrateRepScore", did);
      return venrateRepScore;
    }
  },

  async resolveAndReturnEthaddressFromENS({}, payload) {
    // console.log(payload);
    // var address = await Eth.ens.getAddress(payload.ens);
    // console.log(address);
    Eth.ens
      .resolver(payload.ens)
      .then(address => {
        console.log(address);
        return address;
      })
      .catch(err => {
        console.error(err);
        return null;
      });
  },
  async logout({ commit }) {
    setTimeout(() => {
      commit("logout");
    }, 1000);
  }
};

// async function venrateInfuraUpload(selectedImage) {
//   let imgurl = null;

//   if (selectedImage != null) {
//     const file = await ipfs.add(selectedImage);
//     console.log(file);
//     imgurl = "https://ipfs.io/ipfs/" + file.path;
//     console.log('img url' + imgurl);
//   }
//   return imgurl
// }
