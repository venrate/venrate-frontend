export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Venrate",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/logos/Logo.svg" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600&display=swap"
      },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500&display=swap"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["vuesax/dist/vuesax.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ["@/plugins/vuesax"],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // [
    //   "vue-fontawesome",
    //   {
    //     imports: [
    //       {
    //         set: "@fortawesome/free-solid-svg-icons",
    //         icons: ["fas"]
    //       },
    //       {
    //         set: "@fortawesome/free-brands-svg-icons",
    //         icons: ["fab"]
    //       }
    //     ]
    //   }
    // ]
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  watchers: {
    webpack: {
      poll: true
    }
  }
};
